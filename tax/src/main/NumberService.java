package main;

import java.util.HashMap;
import java.util.Map;

public class NumberService {

    public final static Map<String, Long> numberSmsMap = new HashMap<>();


    public static boolean numberIsValid(String userNumberToLogIn) {
        if(userNumberToLogIn != null && userNumberToLogIn.matches("\\d{7,8}")){
            return true;
        }
        throw new IllegalArgumentException("Number is not valid.");
    }

    public static void addSmsCodeToNumber(String userNumberToLogIn, long generateSmsCode) {
        numberSmsMap.put(userNumberToLogIn, generateSmsCode);
    }

    public static boolean validateNumberAndSmsCode(String userNumberToLogIn, Long code) {
        if( code  != null && numberSmsMap.containsKey(userNumberToLogIn)){
            Long aLong = numberSmsMap.get(userNumberToLogIn);
            return aLong.equals(code);
        }
        throw new IllegalArgumentException( "Code is null or number is not in map" );
    }
}
