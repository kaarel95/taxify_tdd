package main;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DatabaseService.fillUserDb();
        // Available users:
        //("Jaak", "5664123");
        //("Kaak", "53424234");
        //("Maara", "5511231");
        //("Anne", "5653133");
        //("Ilmar", "5667482")

        while(true) {
            System.out.println("----Enter the phone number----" +
                    "\n(no special characters allowed, phone length is 7-8 numbers)");
            Scanner scan= new Scanner(System.in);
            String phoneNumber = scan.nextLine();

            try {
                NumberService.numberIsValid(phoneNumber);
            }
            catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                continue;
            }

            try {
                DatabaseService.doesUserExist(phoneNumber);
            }
            catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
                continue;
            }

            while (true) {
                long smsCode = SmsService.generateSmsCode();
                NumberService.addSmsCodeToNumber( phoneNumber, smsCode);

                System.out.println("\nThe phone is correct! Your login code is: " + smsCode);
                System.out.println("\nEnter the login code");

                try {
                    long codeConfirmation = new Long(scan.nextLine());
                    if (NumberService.numberSmsMap.get(phoneNumber) == codeConfirmation) {
                        User user = DatabaseService.getUser(phoneNumber);
                        System.out.println("\nThe login code is correct.");
                        System.out.println("\nHELLO, " + user.getName());
                        break;
                    }
                    else {
                        System.out.println("\nThe login code is not correct. Please try again.");
                        continue;
                    }
                }
                catch (Exception ex){
                    System.out.println("\nThe login code is not correct. Please try again.");
                    continue;
                }
            }

            break;
        }
    }
}
