package main;

import java.util.ArrayList;

public class DatabaseService {
    public static ArrayList<User> userDb;

    public static void fillUserDb() {
        userDb = new ArrayList<>();
        User user1 = new User("Jaak", "5664123");
        User user2 = new User("Kaak", "53424234");
        User user3 = new User("Maara", "5511231");
        User user4 = new User("Anne", "5653133");
        User user5 = new User("Ilmar", "5667482");
        userDb.add(user1);
        userDb.add(user2);
        userDb.add(user3);
        userDb.add(user4);
        userDb.add(user5);
    }

    public static boolean doesUserExist(String userNumberToLogIn) {
        User user = userDb.stream().filter(x -> x.getPhoneNumber().equals(userNumberToLogIn)).findFirst().orElse(null);
        if(user != null){
            return true;
        }
        throw new IllegalArgumentException ("User with this number does not exist");
    }

    public  static User getUser(String userNumberToLogIn) {
        return userDb.stream().filter(x -> x.getPhoneNumber().equals(userNumberToLogIn)).findFirst().orElse(null);
    }
}
