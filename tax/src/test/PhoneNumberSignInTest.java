package test;

import main.DatabaseService;
import main.NumberService;
import main.SmsService;
import main.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class PhoneNumberSignInTest {

    private static final String userNumberToLogIn = "58827263";
    private static final String incorrectUserNumber = "-9a";
    private static final User testUser = new User("Kaarel", "58827263");

    @Before
    public void setUp() {
        DatabaseService.fillUserDb();
    }


    @Test(expected = IllegalArgumentException.class)
    public void userDoesNotExistInDatabase() {
        DatabaseService.doesUserExist(testUser.getPhoneNumber());
    }

    @Test
    public void userDoesExistInDatabase() {
        DatabaseService.userDb.add(testUser);
        assertTrue(DatabaseService.doesUserExist(testUser.getPhoneNumber()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void enteredNumberFormatIsNotCorrect() {
        NumberService.numberIsValid(incorrectUserNumber);
    }

    @Test
    public void enteredNumberFormatIsCorrect() {
        assertTrue(NumberService.numberIsValid(userNumberToLogIn));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullNumberFormatIsNotCorrect() {
        NumberService.numberIsValid(null);
    }

    @Test
    public void generatedSmsCodeIsAddedToDb(){
        NumberService.addSmsCodeToNumber( userNumberToLogIn, SmsService.generateSmsCode());
        assertTrue(NumberService.numberSmsMap.containsKey( userNumberToLogIn ));
    }

    @Test
    public void generatedSmsCodeIsLinkedToCorrectNumber(){
        long smsCode = SmsService.generateSmsCode();
        NumberService.addSmsCodeToNumber( userNumberToLogIn, smsCode);
        assertTrue( NumberService.numberSmsMap.get(userNumberToLogIn) == smsCode);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateCodeAndNumberWhenCodeIsNull(){
        long smsCode = SmsService.generateSmsCode();
        NumberService.addSmsCodeToNumber( userNumberToLogIn, smsCode);
        NumberService.validateNumberAndSmsCode( userNumberToLogIn, null );
    }

    @Test
    public void validateCodeAndNumberWhenWrongCode(){
        long smsCode = SmsService.generateSmsCode();
        NumberService.addSmsCodeToNumber( userNumberToLogIn, smsCode);
        assertFalse(NumberService.validateNumberAndSmsCode( userNumberToLogIn, 1234L ));
    }

    @Test
    public void validateCodeAndNumberWhenCodeIsLinkedCorrectly(){
        long smsCode = SmsService.generateSmsCode();
        NumberService.addSmsCodeToNumber( userNumberToLogIn, smsCode);
        assertTrue(NumberService.validateNumberAndSmsCode( userNumberToLogIn, smsCode ));
    }


}
